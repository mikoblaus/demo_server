const jwtDecode = require('jwt-decode')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
require('dotenv').config()

const User = require('../models/User');
const utils = require('../utils')

module.exports.signup = async (req, res) => {
    try {
        const email = req.body.email.toLowerCase()
        const userName = utils.ucFirst(req.body.name.toLowerCase())
        const userGender = utils.ucFirst(req.body.gender.toLowerCase())
        const candidate = await User.findOne({email: email});

        if (candidate) {
            res.status(400).json({
                success: false,
                message: 'This e-mail is already registered'
            })
        } else {
            let salt = bcrypt.genSaltSync(10);
            let password = req.body.password;
            await new User({
                email: email,
                password: bcrypt.hashSync(password, salt),
                salt: salt,
                information: {
                    name: userName,
                    gender: userGender,
                }
            }).save((err, user) => {
                if (err) throw err
                res.status(201).json({
                    success: true,
                    user
                })
            })
        }
    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
module.exports.signin = async (req, res) => {
    try {
        const email = req.body.SignInEmail.toLowerCase()
        const password = req.body.SignInPassword
        const candidate = await User.findOne({email: email})

        if (candidate) {
            const passwordResult = bcrypt.compareSync(password, candidate.password)
            if (passwordResult) {
                const token = await jwt.sign({
                    email: candidate.email,
                    userId: candidate._id
                }, process.env.JWT_SECRET_KEY)

                res.status(200).json({
                    success: true,
                    candidate: candidate,
                    token: token
                })
            } else {
                res.status(400).json({
                    success: false,
                    message: 'email_or_password_incorect'
                })
            }
        } else {
            res.status(400).json({
                success: false,
                message: 'email_or_password_incorect'
            })
        }
    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}

module.exports.getUserInformation = async (req, res) => {
    try {
        const userId = jwtDecode(req.headers.authorization).userId;
        const candidate = await User.findOne({_id: userId});
        if (candidate) {
            res.status(201).json({
                success: true,
                candidate: candidate
            })
        } else {
            res.status(400).json({
                success: false,
                message: 'An error occurred, please try again'
            })
        }

    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
