const jwtDecode = require('jwt-decode')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
require('dotenv').config()

const User = require('../models/User');
const ChatRoom = require('../models/ChatRoom')
const Message = require('../models/Message')

module.exports.addUser = async (req, res) => {
    try {
        const userId = jwtDecode(req.headers.authorization).userId;

        await new ChatRoom({
            from: userId,
            to: req.body.user,
        }).save((err, result) => {
            if (err) throw err
            res.status(201).json({
                success: true,
                result
            })
        })


    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
module.exports.chatrooms = async (req, res) => {
    try {
        const userId = jwtDecode(req.headers.authorization).userId;
        let result = await ChatRoom.find({
            $or: [{
                from: userId,
            }, {
                to: userId,
            }]
        }).sort({createdAt: -1})

        let contactId;
        for (let a = 0; a < result.length; a++) {
            let b = await ChatRoom.find({
                _id: result[a]._id
            })
            if (b[0].from === userId) {
                contactId = b[0].to;
            } else {
                contactId = b[0].from;
            }
            let user = await User.findOne({_id: contactId}, {
                information: 1,
                actionDate: 1,
                _id: 1
            }) ? await User.findOne({_id: contactId}, {information: 1, actionDate: 1, _id: 1}) : null
            result[a].additionalData = {
                user
            }
        }
        res.status(201).json({
            success: true,
            result
        })
    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
module.exports.messages = async (req, res) => {
    try {
        const userId = jwtDecode(req.headers.authorization).userId;
        const chatRoomId = req.params.chatRoomId;
        await Message.find({
                chatRoomId: chatRoomId
            },
            async function (err, result) {
                if (err) throw err
                res.status(201).json({
                    success: true,
                    result
                })
            }
        ).sort({_id: -1})

    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
