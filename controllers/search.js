const jwtDecode = require('jwt-decode')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
require('dotenv').config()

const User = require('../models/User');
const utils = require('../utils')

module.exports.getUsers = async (req, res) => {
    try {
        const userId = jwtDecode(req.headers.authorization).userId;
        const skip = parseInt(req.params.skip)
        const limit = parseInt(req.params.limit)

        let users = await User.find({ _id : {$ne : userId}}).sort({_id: 1})
            .skip(skip)
            .limit(limit)

        if (users) {
            res.status(201).json({
                success: true,
                users,
            })
        }

    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}

module.exports.getUserValue = async (req, res) => {
    try {
        const value = req.params.value
        const userId = jwtDecode(req.headers.authorization).userId;
        let users = await User.find({
            $or: [{
                email: value,
            }, {
                'information.name': value
            }]
        })

        if (users) {
            res.status(201).json({
                success: true,
                users,
            })
        }

    } catch (e) {
        res.status(500).json({
            success: false,
            message: 'Something broke!'
        })
    }
}
