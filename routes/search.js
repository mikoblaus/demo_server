const express = require('express');
const passport = require('passport')
const controller = require('../controllers/search');

const router = express.Router();

router.get('/getUsers', passport.authenticate('jwt', {session:false}), controller.getUsers);
router.get('/getUser/:value', passport.authenticate('jwt', {session:false}), controller.getUserValue);

module.exports = router;
