const express = require('express');
const passport = require('passport')
const controller = require('../controllers/authorization');

const router = express.Router();

router.post('/signup', controller.signup);
router.post('/signin', controller.signin);
router.get('/getUserInformation', passport.authenticate('jwt', {session:false}),  controller.getUserInformation);
// router.post('/resetpassword', controller.resetpassword);

module.exports = router;
