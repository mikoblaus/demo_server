const express = require('express');
const passport = require('passport')
const controller = require('../controllers/chat');

const router = express.Router();

router.post('/addUser',  passport.authenticate('jwt', {session:false}), controller.addUser);
router.get('/chatrooms', passport.authenticate('jwt', {session:false}),   controller.chatrooms);
router.get('/messages/:chatRoomId', passport.authenticate('jwt', {session:false}),   controller.messages);

module.exports = router;
