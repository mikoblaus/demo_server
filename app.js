const express = require('express')
const app = express()
const http = require('http');
const server = http.createServer(app);
const options = {
    cors: {
        origin: '*',
    },
};
const { Server } = require("socket.io");
const io = new Server(server, options);

const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const passport = require('passport')
require('dotenv').config()

app.use(cors())
app.use(morgan("dev"))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(passport.initialize())

require('./middleware/auth')(passport)

mongoose.connect(process.env.MONGODB, {useNewUrlParser: true})
    .then(() => console.log('MongoDB connection success'))
    .catch(error => console.log(error))

const authorization = require('./routes/authorization')
const search = require('./routes/search')
const chat = require('./routes/chat')

app.use('/api/auth', authorization);
app.use('/api/search', search);
app.use('/api/chat', chat);

const {GetChatRooms, CreateMessage} = require('./socket/chat')

io.on('connection', async(socket) =>{
    socket.on('send_message',  (data) =>{
        CreateMessage(data).then(value =>{
            io.sockets.emit('update_messanger',value)
        })
    })

    socket.on('disconnect', () =>{
        console.log("User Disconnected")
    })
})

// webRTC
io.on("connection", (socket) => {
    socket.emit("socketId", socket.id);

    socket.on("disconnect", () => {
        socket.broadcast.emit("callEnded")
    })

    socket.on('callUser', (data) => {
        io.sockets.emit('reciveCall', {
            socketId : data.socketId,
            name : data.name,
            callerName : data.callerName,
            chatRoomId : data.chatRoomId,
            userId : data.userId,
            signal : data.signalData,
        })
    })
    socket.on("answerCall", (data) => {
        io.to(data.to).emit("callAccepted", data.signal)
    })
})


server.listen(process.env.SERVER_PORT, (err) => {
    console.log(`Connect server port: ${process.env.SERVER_PORT}`)
})
