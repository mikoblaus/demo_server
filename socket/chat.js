const ChatRoom = require('../models/ChatRoom');
const Message = require('../models/Message');

exports.GetChatRooms = async (myId) => {

}

exports.CreateMessage = async (data) => {
    return await new Message({
        chatRoomId: data.item.chatRoomId,
        userId: data.item.userId,
        message: data.item.message,
    }).save()
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}


