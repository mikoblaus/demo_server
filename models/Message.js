const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Message = new Schema({
    chatRoomId : {type:String, },
    userId : {type:String,  },
    message : {type:String, },
    messageStatus : {type:String },
    see : {type:Number, default: 0},
    createdAt : { type: Date, default: new Date()}
})

module.exports = mongoose.model('Message', Message)
