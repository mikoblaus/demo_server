const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ChatRoom = new Schema({
    from : {type:String, required:true},
    to : {type:String, required:true},
    status : {type:Number,default:0},
    additionalData : {type:Object},
    createdAt : { type: Date, default: new Date()},
    updated_at: { type: Date, default: new Date()}
})

module.exports = mongoose.model('ChatRoom', ChatRoom)
