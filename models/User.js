const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
    email : {
        type:String,
        required:true,
        unique:true
    },
    password : {type:String},
    refreshToken : {type:String},
    salt : {type:String},
    information : {
        name : {type:String},
        gender : {type:String},
    },
    createdAt : { type: Date, default: new Date()}
})

module.exports = mongoose.model('User', User)
